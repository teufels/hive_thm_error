<?php
if(!defined('TYPO3_MODE')){
    die('Access denied.');
}

/***************
 * Make the extension configuration accessible
 */
if(!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY])){
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY] = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
}

/*
 * Default 404 Page Fallback in EXT:hive_thm_error
 *
 * Override e.g. by setting another path in AdditionalConfiguration.php
 * or set page id via $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectPageID'] = x;
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectFallback'] = '/typo3conf/ext/hive_thm_error/Resources/Public/404/';

/*
 * User function to render 404 page
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = 'USER_FUNCTION:typo3conf/ext/hive_thm_error/Classes/Utility/PageNotFoundHandling.php:HIVE\HiveThmError\Utility\PageNotFoundHandling->pageNotFound';

/***************
 * Reset extConf array to avoid errors
 */
if(is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY])){
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY] = serialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
}
